#!/usr/bin/python3
import multiprocessing as mp
import math
ncpu=mp.cpu_count()

foundprimes=[2]
batchsize=200_000
searchlen=3_000_000

def testdivisors():
   """ generator to create testdivisors 
       takes already found primes from foundprimes
       if they are exausted adds 2 to the last one
   """
   for t in foundprimes:
      yield t
   while True:
     t+=2
     #print("slow ",t)
     yield t   



def findprimes(ab): 
    """ ab is a tuple a,b
        return a list of all primes between a and b-1 
        uses the global "foundprimes" to use for testing
        new primes
        if the list is not large enough test by
        counting from the last one up 
    """
    # performance optimization
    # to avoid testing even numbers find the first odd number after a
    # and then count up from there in steps of 2
    a,b=ab
    odd_after_a= a+1 if a%2 == 0 else a
    found=[]
    for n in range(odd_after_a,b,2):
      nsqrt=int(math.sqrt(n)+1)
      for p in testdivisors():
        if p > nsqrt:
          found.append(n)
          break
        if n % p == 0:
          break
    return found

foundprimes.extend( findprimes((3,30)) )    
foundprimes.extend( findprimes((30,1000)) )
foundprimes.extend( findprimes((1000,batchsize)) )

last=batchsize


# we could swap these 2 loops: then we would not need to create
# new processes in each itteration but at the cost of
# not taking the list of found primes into the new process

while len(foundprimes) < searchlen:
  with mp.Pool(ncpu) as pool:
    ranges=[(last+k*batchsize,last+(k+1)*batchsize) for k in range(ncpu)]
    res=pool.map(findprimes,ranges)
    for r in res:
      foundprimes.extend(r)
    last=ranges[-1][1]
    #print(len(foundprimes),foundprimes[-1])
print(f"the {searchlen}th prime is",foundprimes[searchlen-1])

